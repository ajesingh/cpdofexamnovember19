package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */
public class MinMaxTest {

    @Test
    public void findTest() {

        assertEquals("Find Max", 10, new MinMax().find(10,5));
        assertEquals("Find Max", 10, new MinMax().find(5,10));
        assertEquals("Find Max", 10, new MinMax().find(10,10));
    }
}

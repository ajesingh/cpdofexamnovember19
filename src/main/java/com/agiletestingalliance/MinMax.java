package com.agiletestingalliance;

public class MinMax {

    public int find(int value1, int value2) {
        if (value2 > value1) {
            return value2;
        } else {
            return value1;
        }
    }

}
